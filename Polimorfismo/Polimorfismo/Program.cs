﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polimorfismo
{
	class Program
	{
		public class Perro
		{
			public string Ladrar()
			{
				return "Perro Ladrando";
			}
		}
		public class Chihuaha : Perro
		{
			public new string Ladrar()
			{
				return "Mi perro es un Chihuaha";
			}

		}
		public class HUsky : Perro
		{
			public new string Ladrar()
			{
				return "Mi perro es un Husky";
			}
		}
		public class Labrador : Perro { }


		static void Main(string[] args)
		{
			/* 
			 * todas las clases estan heredando de una misma clase y usando el mismo metodo pero re interpretandolo segun su raza de perro
			 * el polimorfismo esta aplicandose ya que cada clase que esta herdeando de la clase padre ( Perro ) responden al mismo mensaje pero 
			 * reiterpretandose con valores diferentes de la clase que esta siendo heredada
			*/

			Labrador labrador = new Labrador();
			Chihuaha chihuaha = new Chihuaha();
			HUsky husky = new HUsky();

			Console.WriteLine(husky.Ladrar());

			Console.WriteLine(chihuaha.Ladrar());

			Console.WriteLine(labrador.Ladrar());
			Console.ReadKey();

		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using MetroFramework;
using MySql.Data;
using CRUD;
namespace CRUD
{
	public partial class UNIDAD_2 : MetroFramework.Forms.MetroForm
	{
		CRUD cRUD = new CRUD();
		int cont = 1;

		public UNIDAD_2()
		{
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			Grid1.DataSource = cRUD.ListaData();
		}

		private void btnAgregar_Click(object sender, EventArgs e)
		{
			if (cRUD.Insert(txtBox1.Text))
			{
				MetroMessageBox.Show(this, "Nuevo Usuario: " + txtBox1.Text + "Fue Registrado", "Usuario Registrado", MessageBoxButtons.OK, MessageBoxIcon.Question);
			}
			else
			{
				MetroMessageBox.Show(this, "Error al Ingresar Usuario: " + txtBox1.Text, "Error al Registrar", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void btnActualizar_Click(object sender, EventArgs e)
		{
			if (cRUD.Update(cont, txtBox2.Text))
			{
				MetroMessageBox.Show(this, "Usuario: "+ txtBox2.Text +" Actualizado", "Usuario Actualizado", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			else
			{
				MetroMessageBox.Show(this, "Error al Actualizar al Usuario: " + txtBox2.Text, "Usuario al Actualizar", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void btnEliminar_Click(object sender, EventArgs e)
		{
			if (cRUD.Update(cont, txtBox2.Text))
			{
				MetroMessageBox.Show(this, "Usuario: " + txtBox2.Text + "Usuario Eliminado","Registro Eliminado", MessageBoxButtons.OK, MessageBoxIcon.Question);
			}
			else
			{
				MetroMessageBox.Show(this,"Error al Eliminar el Usuario: " + txtBox2.Text, "Error al Eliminar", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void Grid1_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			txtBox2.Text = Grid1.CurrentRow.Cells[1].Value.ToString();
			cont = Convert.ToInt16(Grid1.CurrentRow.Cells[0].Value.ToString());
		}
	}
}
